using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.UI
{
    public class HeathBar : MonoBehaviour
    {
        [SerializeField] private Image fillBar;
        [SerializeField] private TextMeshProUGUI textHeath;
    
        
        public void UpdateHeathBar(int currentHeath, int maxHeath)
        {
            fillBar.fillAmount = (float) currentHeath/ maxHeath;
            textHeath.text = $"{currentHeath}/{maxHeath}";
        }
    }
}
