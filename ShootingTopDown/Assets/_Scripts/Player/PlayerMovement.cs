using System.Collections;
using UnityEngine;

namespace _Scripts.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        [SerializeField] private Rigidbody2D rigi2D;
        [SerializeField] private SpriteRenderer spriteRenderer;
        
        
        [Header("Speed")]
        [SerializeField] private float moveSpeed = 5f;
        [SerializeField] private Vector3 moveInput;
        
        [Header("Dash")]
        [SerializeField] private float dashBoost;
        [SerializeField] private float dashTime;
        [SerializeField] private float dashCooldownTime;

        [Header("Shost Effect")] 
        [SerializeField] private GameObject ghostEffect;
        [SerializeField] private float ghostDelayTime;
        private Coroutine _dashCoroutine;

        private float _currentDashTime;
        private float _currentDashCooldownTime;
        private bool _isDash;
        

        // Update is called once per frame
        void Update()
        {
            this.moveInput.x = Input.GetAxis("Horizontal");
            this.moveInput.y = Input.GetAxis("Vertical");

            this.rigi2D.velocity = moveInput * this.moveSpeed;

            if (Input.GetButtonDown("Jump") && this._currentDashTime <= 0 && this._currentDashCooldownTime <= 0)
            {
                this.moveSpeed += this.dashBoost;
                this._currentDashTime = this.dashTime;
                this._currentDashCooldownTime = this.dashCooldownTime;
                this._isDash = true;
                StartDashEffect();
            }

            if (this._currentDashTime <= 0 && this._isDash)
            {
                this.moveSpeed -= this.dashBoost;
                this._isDash = false;
                StopDashEffect();
            }
            else
            {
                this._currentDashTime -= Time.deltaTime;
                this._currentDashCooldownTime -= Time.deltaTime;
            }

            CheckFlip();
            UpdateMovingAnimation();
        }

        private void CheckFlip()
        {
            Vector3 scale = this.transform.localScale;
            if (this.moveInput.x != 0)
            {
                if (this.moveInput.x < 0)
                {
                    this.spriteRenderer.transform.localScale = new Vector3(-Mathf.Abs(scale.x), scale.y, scale.z);
                }
                else
                {
                    this.spriteRenderer.transform.localScale = new Vector3(Mathf.Abs(scale.x), scale.y, scale.z);
                }
            }
        }

        private void UpdateMovingAnimation()
        {
            if (this.moveInput.x != 0 || this.moveInput.y != 0)
            {
                this.animator.SetBool("isWalk", true);
            }
            else
            {
                this.animator.SetBool("isWalk", false);
            }
        }

        private void StopDashEffect()
        {
            if (_dashCoroutine !=  null)
            {
                StopCoroutine(_dashCoroutine);
            }
        }
        private void StartDashEffect()
        {
            if (_dashCoroutine !=  null)
            {
                StopCoroutine(_dashCoroutine);
            }
            _dashCoroutine = StartCoroutine(DashEffectCoroutine());
        }
        IEnumerator DashEffectCoroutine()
        {
            while (true)
            {
                GameObject _ghost = Instantiate(ghostEffect, this.transform.position, this.transform.rotation);
                Destroy(_ghost,0.5f);
                yield return new WaitForSeconds(ghostDelayTime);
            }
        }
    }
}