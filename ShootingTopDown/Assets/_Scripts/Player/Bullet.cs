using UnityEngine;

namespace _Scripts.Player
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D bulletRgb2D;
        [SerializeField] private float bulletForce; // luc di chuyen cua dan

        public Transform gun;
        

        // Update is called once per frame
        void Update()
        {
          
            bulletRgb2D.AddForce(this.gun.right* bulletForce,ForceMode2D.Impulse);
           
        }

       
    }
}
