using _Scripts.UI;
using UnityEngine;

namespace _Scripts.Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private HeathBar heathBar;
        [SerializeField] private int maxHp;
        [SerializeField] private Animator animator;
        [SerializeField] private CircleCollider2D collider;

        private int _currentHp;
        
        // Start is called before the first frame update
        void Start()
        {
            _currentHp = maxHp;
            heathBar.UpdateHeathBar(_currentHp,maxHp);
        }

        // Update is called once per frame
        void Update()
        {
            
            if (Input.GetButtonDown("Jump") )
            {
                ReceiveDamage(10);
            }
        }

        // nhan bao nhieu dmg thi tru bay nhieu hp hien tai
        public void ReceiveDamage(int dmg)
        {
            _currentHp -= dmg;
            heathBar.UpdateHeathBar(_currentHp,maxHp);
            if (IsDeath())
            {
                animator.SetTrigger("death");
                collider.enabled = false;
                PlayerDeath();
            }
        }

        public bool IsDeath()
        {
            return _currentHp <= 0;
        }
        
        
        public void PlayerDeath()
        {
           Destroy(gameObject,1.5f);
        }
    }
}
