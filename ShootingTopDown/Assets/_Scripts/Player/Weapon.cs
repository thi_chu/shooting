using System.Collections.Generic;
using UnityEngine;

namespace _Scripts.Player
{
    public class Weapon : MonoBehaviour
    {
        [SerializeField] private GameObject spark;
        [SerializeField] private GameObject bullet;
        [SerializeField] private Transform bulletPos;
        [SerializeField] private float timeBtwShoot = 0.2f; // khoang thoi gian ban giua 2 vien dan

        private float _currentTimeBtwShoot;
        private Camera _camera;

        // Update is called once per frame
        private void Awake()
        {
            this._camera = Camera.main;
        }

        void Update()
        {
            RotateGun();
            this._currentTimeBtwShoot -= Time.deltaTime;
            if (Input.GetMouseButtonDown(0) && _currentTimeBtwShoot <= 0)
            {
                Shooting();
            }
        }

        /// <summary>
        /// sung quay huong theo con tro chuot
        /// mousePos - doi 1 diem tren man hinh sang he toa do tren hierarchy
        /// lookDir - lay huong quay cho sung tu vi tri hien tai va diem chuot tro toi
        /// angle - do lon goc quay cua sung
        ///
        /// Mathf.Atan2 -> tra ve goc theo radian co Tan = y/x
        /// Mathf.Rad2Deg -> chuyen doi tu radian sang do
        /// </summary>
        private void RotateGun()
        {
            Vector3 mousePos = this._camera.ScreenToWorldPoint(Input.mousePosition);
            Vector2 lookDir = mousePos - this.transform.position;
            float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;
            Quaternion rotation = Quaternion.Euler(0, 0, angle);

            this.transform.rotation = rotation;
            
            if (this.transform.eulerAngles.z > 90 && this.transform.eulerAngles.z < 270)
            {
              
                this.transform.localScale = new Vector3(1, -1, 1);
            }
            else
            {
                this.transform.localScale = new Vector3(1, 1, 1);
            }
        }

        private void Shooting()
        {
            this._currentTimeBtwShoot = this.timeBtwShoot;
            GameObject _bulletTemp = Instantiate(this.bullet,this.bulletPos.position,Quaternion.identity);
            _bulletTemp.GetComponent<Bullet>().gun = this.transform;
            // effect
            Instantiate(this.spark,this.bulletPos.position,this.transform.rotation, this.transform);
        }
    }
}