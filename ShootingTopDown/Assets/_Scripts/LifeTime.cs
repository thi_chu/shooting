using System;
using UnityEngine;

namespace _Scripts
{
    public class LifeTime : MonoBehaviour
    {
        [SerializeField] private float lifeTime;

        private void Start()
        {
            Destroy(gameObject,lifeTime);
        }
    }
}